#include "geometrica.cpp"
#include "retangulo.cpp"
#include "triangulo.cpp"
#include <iostream>

using namespace std;

int main(){
	Retangulo *umRetangulo=new Retangulo(25, 40);
	Triangulo umTriangulo;

	cout<<"A area do retangulo eh "<<umRetangulo->Area()<<endl;
	cout<<"A area do triangulo eh "<<umTriangulo.Area()<<endl;

	return 0;
}
