#ifndef _GEOMETRICA_H_
#define _GEOMETRICA_H_

#include <iostream>

using namespace std;

class Geometrica{
	private:
		float altura;
		float base;

	public:
		Geometrica();
		Geometrica(float altura, float base);
		float getAltura();
		float getBase();
		void setAltura(float altura);
		void setBase(float base);

		virtual float Area()=0;
};

#endif
