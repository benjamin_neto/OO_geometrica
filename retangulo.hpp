#ifndef _RETANGULO_H_
#define _RETANGULO_H_

#include "geometrica.hpp"

class Retangulo:public Geometrica{
	public:
		Retangulo();
		Retangulo(float altura, float base);
		float Area();
		float Area(float altura, float base);
};

#endif
