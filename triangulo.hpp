#ifndef _TRIANGULO_H_
#define _TRIANGULO_H_

#include "geometrica.hpp"

class Triangulo:public Geometrica{
	public:
		Triangulo();
		Triangulo(float altura, float base);
		float Area();
		float Area(float altura, float base);
};

#endif
