#include "geometrica.hpp"

using namespace std;

Geometrica::Geometrica(){
	altura=0;
	base=0;
}

Geometrica::Geometrica(float altura, float base){
	this->altura=altura;
	this->base=base;
}

float Geometrica::getAltura(){
	return altura;
}

float Geometrica::getBase(){
	return base;
}

void Geometrica::setAltura(float altura){
	this->altura=altura;
}

void Geometrica::setBase(float base){
	this->base=base;
}


