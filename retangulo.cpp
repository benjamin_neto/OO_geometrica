#include "retangulo.hpp"

using namespace std;

Retangulo::Retangulo(){
	setAltura(10);
	setBase(10);
}

Retangulo::Retangulo(float altura, float base){
	setAltura(altura);
	setBase(base);
}

float Retangulo::Area(){
	return getAltura()*getBase();
}

float Retangulo::Area(float altura, float base){
	return altura*base;
}

